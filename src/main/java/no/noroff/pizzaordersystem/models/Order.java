package no.noroff.pizzaordersystem.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Order {
    ArrayList<Pizza> pizzas = new ArrayList<>();
    String customerName = "No customer name registered";
    String customerAddress = "No address registered";

    public Order(){

    }

    public void setCustomerName(String customerName){
        this.customerName = customerName;
    }

    public void setCustomerAddress(String customerAddress){
        this.customerAddress = customerAddress;
    }

    public void create(int j, Integer numberMargarita, Integer numberHawaiian, Integer numberCapricciosa, Integer numberPepperoniHeaven){
        if (numberMargarita != null){
            for(int i = 0; i <numberMargarita; i++){
                ArrayList<String> toppings = new ArrayList<>();
                toppings.add("Cheese"); toppings.add("Tomato sauce");
                Pizza p = new Pizza("margarita", j, toppings);
                pizzas.add(p);
                j++;
            }
        }
        if (numberHawaiian != null){
            for(int i = 0; i <numberHawaiian; i++){
                ArrayList<String> toppings = new ArrayList<>();
                toppings.add("Cheese"); toppings.add("Tomato sauce"); toppings.add("Ham"); toppings.add("Pineapple");
                Pizza p = new Pizza("hawaiian", j, toppings);
                pizzas.add(p);
                j++;
            }
        }

        if (numberCapricciosa != null) {
            for(int i = 0; i <numberCapricciosa; i++){
                ArrayList<String> toppings = new ArrayList<>();
                toppings.add("Cheese"); toppings.add("Tomato sauce"); toppings.add("Ham"); toppings.add("Mushroom");
                Pizza p = new Pizza("capricciosa", j, toppings);
                pizzas.add(p);
                j++;
            }
        }

        if (numberPepperoniHeaven != null) {
            for(int i = 0; i <numberPepperoniHeaven; i++){
                ArrayList<String> toppings = new ArrayList<>();
                toppings.add("Cheese"); toppings.add("Tomato sauce"); toppings.add("Pepperoni");
                Pizza p = new Pizza("pepperoni heaven", j, toppings);
                pizzas.add(p);
                j++;
            }
        }

    }

    public int getIdOfElementOnIndex(int index){
        System.out.println(index);
        return pizzas.get(index-1).getId();
    }

    public ArrayList<Pizza> getPizzas(){
        return this.pizzas;
    }

    public String getCustomerName(){
        return this.customerName;
    }

    public String getCustomerAddress(){
        return this.customerAddress;
    }

    public void delete(){
        pizzas.clear();
    }

    public void printPizzas(){
        System.out.println("Order:");
        for(Pizza p : pizzas){
            System.out.println(p.getName());
        }
    }
}
