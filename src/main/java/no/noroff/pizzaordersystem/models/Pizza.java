package no.noroff.pizzaordersystem.models;

import java.util.ArrayList;

public class Pizza {
    Integer id = null;
    String name = "No name";
    String size = null;
    ArrayList<String> toppings = new ArrayList<>();
    public float price = 0;


    public Pizza(){
    }

    public Pizza(String name, int id, ArrayList<String> toppings){
        this.name = name;
        this.id = id;
        this.toppings = toppings;
    }
/*  //Change toppings klikker hvis denne brukes
    public float getPrice() {
        return this.price;
    }*/

    public void setPrice(float price){
        System.out.println(price);
        this.price = price;
    }

    public String getSize(){
        return this.size;
    }

    public void setSize(String size){
        this.size = size;
    }

    public String getName(){
        return this.name;
    }

    public int getId(){
        return this.id;
    }

    public ArrayList<String> getToppings(){
        return this.toppings;
    }

    public void setToppings(ArrayList<String> toppings){
        this.toppings = toppings;
    }
/*
    public float calculatePrice(){
        float basePrice = 0;
        float toppingPrice = 0;
        switch (size){
            case("large"):
                basePrice = 120;
                break;
            case("medium"):
                basePrice = 100;
                break;
            case("small"):
                basePrice = 80;
                break;
            default:
                System.out.println("No size given");
                break;
        }
        toppingPrice = toppings.size()*20;
        return basePrice + toppingPrice;
    }

 */
}
