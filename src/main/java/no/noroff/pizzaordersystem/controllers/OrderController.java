package no.noroff.pizzaordersystem.controllers;

import no.noroff.pizzaordersystem.models.Order;
import no.noroff.pizzaordersystem.models.Pizza;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.lang.reflect.Array;
import java.util.ArrayList;


@RestController
public class OrderController {

    Order order = new Order();

    @RequestMapping("/createOrder")
    public RedirectView createOrder(@RequestParam("margaritaSize") String margaritaSize,
                                    @RequestParam("hawaiianSize") String hawaiianSize,
                                    @RequestParam("capricciosaSize") String capricciosaSize,
                                    @RequestParam("pepperoniHeavenSize") String pepperoniHeavenSize,
                                    @RequestParam("customerName") String customerName,
                                    @RequestParam("customerAddress") String customerAddress,
                                    @RequestParam("margarita") Integer numberMargarita,
                                    @RequestParam("hawaiian") Integer numberHawaiian,
                                    @RequestParam("capricciosa") Integer numberCapricciosa,
                                    @RequestParam("pepperoniHeaven") Integer numberPepperoniHeaven){

        order.setCustomerName(customerName);
        order.setCustomerAddress(customerAddress);

        if (order.getPizzas().size() == 0) {
            order.create(1, numberMargarita, numberHawaiian, numberCapricciosa, numberPepperoniHeaven);

            for(Pizza p : order.getPizzas()){
                switch (p.getName()) {
                    case "margarita":
                        p.setSize(margaritaSize);
                        break;
                    case "hawaiian":
                        p.setSize(hawaiianSize);
                        break;
                    case "capricciosa":
                        p.setSize(capricciosaSize);
                        break;
                    case "pepperoni heaven":
                        p.setSize(pepperoniHeavenSize);
                        break;
                }
            }
            return new RedirectView("/orderCreated.html");
        } else {
            return new RedirectView("/orderAlreadyCreated.html");
        }
    }

    @RequestMapping("/viewOrderInfo")
    public ArrayList<String> viewOrderInfo(){
        ArrayList<String> info = new ArrayList<>();
        info.add(order.getCustomerName());
        info.add(order.getCustomerAddress());
        return info;
    }

    @RequestMapping("/viewOrder")
    public ArrayList<Pizza> viewOrder(){
        return order.getPizzas();
    }

    @RequestMapping("/addPizza")
    public RedirectView addPizza(@RequestParam("margaritaSize") String margaritaSize,
                                 @RequestParam("hawaiianSize") String hawaiianSize,
                                 @RequestParam("capricciosaSize") String capricciosaSize,
                                 @RequestParam("pepperoniHeavenSize") String pepperoniHeavenSize,
                                 @RequestParam("margarita") Integer numberMargarita,
                                 @RequestParam("hawaiian") Integer numberHawaiian,
                                 @RequestParam("capricciosa") Integer numberCapricciosa,
                                 @RequestParam("pepperoniHeaven") Integer numberPepperoniHeaven){
        int orderSize = order.getPizzas().size();

        if(orderSize <= 0){
            int j=1;
            order.create(j, numberMargarita, numberHawaiian, numberCapricciosa, numberPepperoniHeaven);
            for (int i = j-1; i < order.getPizzas().size(); i++){
                switch (order.getPizzas().get(i).getName()) {
                    case "margarita":
                        order.getPizzas().get(i).setSize(margaritaSize);
                        break;
                    case "hawaiian":
                        order.getPizzas().get(i).setSize(hawaiianSize);
                        break;
                    case "capricciosa":
                        order.getPizzas().get(i).setSize(capricciosaSize);
                        break;
                    case "pepperoni heaven":
                        order.getPizzas().get(i).setSize(pepperoniHeavenSize);
                        break;
                }
            }
        } else{
            int j = order.getIdOfElementOnIndex(orderSize)+1;
            System.out.println(j);
            order.create(j, numberMargarita, numberHawaiian, numberCapricciosa, numberPepperoniHeaven);

            for (int i = j-1; i < order.getPizzas().size(); i++){
                switch (order.getPizzas().get(i).getName()) {
                    case "margarita":
                        order.getPizzas().get(i).setSize(margaritaSize);
                        break;
                    case "hawaiian":
                        order.getPizzas().get(i).setSize(hawaiianSize);
                        break;
                    case "capricciosa":
                        order.getPizzas().get(i).setSize(capricciosaSize);
                        break;
                    case "pepperoni heaven":
                        order.getPizzas().get(i).setSize(pepperoniHeavenSize);
                        break;
                }
            }
        }
        return new RedirectView("/pizzaAdded.html");
    }

    @RequestMapping("/removePizza")
    public RedirectView removePizza(@RequestParam("pizzaId") Integer pizzaId) {
        boolean found = false;

        System.out.println(order.getPizzas().size());

        if (order.getPizzas().size() > 0 ){
            for(int i = 0; i < order.getPizzas().size(); i++){
                System.out.println(order.getPizzas().get(i).getId());
                System.out.println(pizzaId);
                if (order.getPizzas().get(i).getId() == pizzaId){
                    order.getPizzas().remove(i);
                    found = true;
                }
            }
        }

        if(found){
            return new RedirectView("/pizzaRemoved.html");
        } else {
            return new RedirectView("/pizzaNotRemoved.html");
        }
    }

    @RequestMapping("/changeTopping")
    public RedirectView changeTopping(@RequestParam("pizzaName") String pizzaName,
                              @RequestParam("pizzaSize") String pizzaSize,
                              @RequestParam(defaultValue = "false") boolean cheeseCheckbox,
                              @RequestParam(defaultValue = "false") boolean tomatoSauceCheckbox,
                              @RequestParam(defaultValue = "false") boolean hamCheckbox,
                              @RequestParam(defaultValue = "false") boolean mushroomCheckbox,
                              @RequestParam(defaultValue = "false") boolean pepperoniCheckbox,
                              @RequestParam(defaultValue = "false") boolean pineappleCheckbox ){

        ArrayList<String> newToppings = new ArrayList<>();

        int pizzaId = 0;

        for (int i = 0; i < order.getPizzas().size(); i++){
            if (order.getPizzas().get(i).getName().equals(pizzaName) && order.getPizzas().get(i).getSize().equals(pizzaSize)){
                pizzaId = i+1;

            }
        }

        if(cheeseCheckbox){
            newToppings.add("Cheese");
        }
        if(tomatoSauceCheckbox){
            newToppings.add("Tomato sauce");
        }
        if(hamCheckbox){
            newToppings.add("Ham");
        }
        if(mushroomCheckbox){
            newToppings.add("Mushroom");
        }
        if(pepperoniCheckbox){
            newToppings.add("Pepperoni");
        }
        if(pineappleCheckbox){
            newToppings.add("Pineapple");
        }

        boolean found = false;

        for(int i = 0; i < order.getPizzas().size(); i++){
            if (order.getPizzas().get(i).getId() == pizzaId){

                Pizza p = order.getPizzas().get(i);
                p.setToppings(newToppings);
                found = true;
            }
        }

        if (found){
            return new RedirectView("/toppingChanged.html");
        } else {
            return new RedirectView("/");
        }
    }

    @RequestMapping("/cancelOrder")
    public RedirectView cancelOrder(){

        if(order.getPizzas().size() == 0){
            return new RedirectView("/noOrdersToCancel.html");
        } else{
            order.delete();
            return new RedirectView("/orderCancelled.html");
        }
    }


    @RequestMapping("/choosePizza")
    public Pizza choosePizza(@RequestParam("pizzaName") String pizzaName,
                            @RequestParam("pizzaSize") String pizzaSize){

        for (Pizza p : order.getPizzas()){
            if (p.getName().equals(pizzaName) && p.getSize().equals(pizzaSize)){
                System.out.println("found pizza");
                return p;
            }
        }
        return null;
    }


    @RequestMapping("/receipt")
    public ArrayList<Pizza> receipt(){
        for(Pizza p : order.getPizzas()){
            //p.setPrice(p.calculatePrice()); //Noe rart skjer når jeg bruker denne
            p.setPrice(calculatePrice(p.getSize(), p.getToppings())); //funker ikke
        }
        return order.getPizzas();
    }

    public float calculatePrice(String size, ArrayList<String> toppings){
        float basePrice = 0;
        float toppingPrice = 0;
        switch (size){
            case("large"):
                basePrice = 120;
                break;
            case("medium"):
                basePrice = 100;
                break;
            case("small"):
                basePrice = 80;
                break;
            default:
                System.out.println("No size given");
                break;
        }
        toppingPrice = toppings.size()*20;
        return basePrice + toppingPrice;
    }

    @RequestMapping("/register")
    public RedirectView register(@RequestParam("customerName") String name,
                                 @RequestParam("customerAddress") String address){
        order.setCustomerName(name);
        order.setCustomerAddress(address);
        return new RedirectView("/");
    }
}
