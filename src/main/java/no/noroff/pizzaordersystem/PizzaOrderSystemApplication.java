package no.noroff.pizzaordersystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import no.noroff.pizzaordersystem.models.Order;


@SpringBootApplication
public class PizzaOrderSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaOrderSystemApplication.class, args);
	}

}
