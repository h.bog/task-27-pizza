function viewOrdersClick(){

    document.getElementById("viewOrdersButton").disabled = true;

    if(document.getElementById("viewOrdersButton").disabled){
        let hideBtn = document.createElement("BUTTON");
        hideBtn.innerText = "Hide order";
        hideBtn.onclick = function(){
            document.getElementById("viewOrdersButton").disabled = false;
            document.getElementById("orders").innerHTML="";
        }
        document.getElementById("orders").appendChild(hideBtn);


    }


            fetch('/viewOrder')
        .then(response => response.json())
        .then(response=>{
            console.log(response);
            if(response.length === 0){
                console.log("response is empty");
                let par  = document.createElement("P");
                par.innerText = "No pizzas have been added to the order yet.";
                document.getElementById("orders").appendChild(par);
            } else{
                response.forEach((pizza)=>{
                    let par  = document.createElement("P");
                    console.log(pizza.id);
                    let text = pizza.id + ". " + pizza.name + ", " + pizza.size + ", Toppings: " ;
                    for(let i = 0; i < pizza.toppings.length; i++){
                        text += pizza.toppings[i] + ", ";
                    }
                    par.innerText = text;
                    //console.log(text);
                    document.getElementById("orders").appendChild(par);
                })
            }
            })
    }